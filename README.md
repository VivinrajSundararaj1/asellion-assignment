# Full Stack Rest Application

Product list display application created using react.js, spring boot and mysql for data persistence

### Tools & Technology Used
**Front End:**
- ReactJS

**Back End:**
- Java 1.8
- Spring Boot
- MySQL 8.0

### HOW TO RUN
Spring boot startup will add 2 chemical compounds into database for demo

To build the docker images to run in separate containers:

	 docker-compose build

To run application:

	 docker-compose up -d

Login to web application with the default user in the below link

	 http://localhost:3000
	 login: vivin
	 password: vivin

Through Postman we can access the API but initially we have to authenticate the user through JWT:

    Input:
      GET Method:
      http://localhost:8080/authenticate

    Body:
        {
          "username":"vivin",
          "password":"vivin"
        }
    Output:
      {
        "token":"XXXXXX"
      }

Copy the JWT token from the output of above login API and now we can access the other REST API's using the JWT Token

    1. To retrieve all the products list
      GET http://localhost:8080/api/products
    2. To retrieve the particular product using the id
      GET http://localhost:8080/api/products/1
    3. To update the particular product using the id
      PUT http://localhost:8080/api/products/1
    4. To create the new product by using the below Method

    POST http://localhost:8080/api/products
    {
          "id": 3,
          "name": "Baking Soda",
          "currentPrice": 20.0,
          "lastUpdatedTime": null
    }

To run the test cases (start back end application locally):

    mvn spring-boot:run(or you can run via Spring Tool Suite or Intellij IDE) or
    java -jar target/asellion-assignment-0.0.1-SNAPSHOT.jar

    mvn test
