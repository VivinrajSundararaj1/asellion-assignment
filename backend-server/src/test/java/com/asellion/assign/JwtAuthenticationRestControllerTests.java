package com.asellion.assign;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.asellion.assign.repository.ProductRepository;
import com.asellion.assign.security.JwtAuthenticationRestController;
import com.asellion.assign.security.JwtTokenUtil;
import com.asellion.assign.security.JwtUnAuthorizedResponseAuthenticationEntryPoint;
import com.asellion.assign.service.ProductService;

@RunWith(SpringRunner.class)
@WebMvcTest(JwtAuthenticationRestController.class)
public class JwtAuthenticationRestControllerTests {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ProductService productService;

	@MockBean
	private ProductRepository productRepository;

	@MockBean
	private UserDetailsService userDetailsService;

	@MockBean
	private JwtTokenUtil jwtTokenUtil;

	@MockBean
	private JwtUnAuthorizedResponseAuthenticationEntryPoint jwtUnAuthorizedResponseAuthenticationEntryPoint;

	@Test
	public void allowAccessToAuthenticatedUsers() throws Exception {
		logger.info("call JWT Authentication api test for authenticated user");
		String token = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ2aXZpbiIsImV4cCI6MTU4NDYzODg1MywiaWF0IjoxNTg0MDM0MDUzfQ.5GwyB3gujfHmb7PJo5gSMauswhYjXJYnc64YDvJ3IjnAyAmirtl9PyKgBVSx4ResBAnT7o44OoaPIZfIt4N2qg";
		this.mockMvc.perform(get("/authenticate").header("Authorization", token).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk());
	}

}
