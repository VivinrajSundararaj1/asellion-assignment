package com.asellion.assign;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.asellion.assign.controller.ProductController;
import com.asellion.assign.repository.ProductRepository;
import com.asellion.assign.security.JwtTokenUtil;
import com.asellion.assign.security.JwtUnAuthorizedResponseAuthenticationEntryPoint;
import com.asellion.assign.service.ProductService;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTests {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private ProductService productService;

	@MockBean
	private ProductRepository productRepository;

	@MockBean
	private UserDetailsService userDetailsService;

	@MockBean
	private JwtTokenUtil jwtTokenUtil;

	@MockBean
	private JwtUnAuthorizedResponseAuthenticationEntryPoint jwtUnAuthorizedResponseAuthenticationEntryPoint;

	public static final String token = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ2aXZpbiIsImV4cCI6MTU4NDYzODg1MywiaWF0IjoxNTg0MDM0MDUzfQ.5GwyB3gujfHmb7PJo5gSMauswhYjXJYnc64YDvJ3IjnAyAmirtl9PyKgBVSx4ResBAnT7o44OoaPIZfIt4N2qg";

	@Test
	public void testGetProducts() throws Exception {
		logger.info("call get products api");
		this.mockMvc.perform(get("/api/products").header("Authorization", token).contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andDo(print()).andExpect(status().isOk());
	}

	@Test
	public void testGetProductById() throws Exception {
		logger.info("call get products by Id api");
		this.mockMvc.perform(get("/api/products/1").header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void testUpdateProductById() throws Exception {
		logger.info("call update products by Id api");
		this.mockMvc.perform(put("/api/products/1").header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk());
	}

	@Test
	public void testCreateProduct() throws Exception {
		logger.info("call update products by Id api");
		this.mockMvc.perform(post("/api/products").header("Authorization", token)
				.contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)).andDo(print())
				.andExpect(status().isOk());
	}
}
