package com.asellion.assign.exception;

/**
 * Class to catch Illegal Argument exception
 *
 */
public class IllegalArgumentException extends RuntimeException {

	public IllegalArgumentException(String message) {
		super(message);
	}
}
