package com.asellion.assign.exception;

/**
 * Class to catch Illegal Operation Exception
 *
 */

public class IllegalOperationException extends RuntimeException {

	public IllegalOperationException(String message) {
		super(message);
	}
}
