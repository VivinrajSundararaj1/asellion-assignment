package com.asellion.assign.exception;

/**
 * Class to catch User Id Not Found Exception
 *
 */

public class NotFoundException extends RuntimeException {

	public NotFoundException(String message) {
		super(message);
	}
}