package com.asellion.assign.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.asellion.assign.entity.Product;
import com.asellion.assign.exception.IllegalOperationException;
import com.asellion.assign.exception.NotFoundException;
import com.asellion.assign.repository.ProductRepository;
import com.asellion.assign.service.ProductService;
import com.asellion.assign.validation.ProductValidationService;

/**
 * Class to handle @{@link Product} related actions
 *
 */

@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	ProductRepository productRepository;
	ProductValidationService productValidationService;

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	public ProductServiceImpl(ProductRepository productRepository, ProductValidationService productValidationService) {
		this.productRepository = productRepository;
		this.productValidationService = productValidationService;
	}

	/**
	 * Method to return all the products
	 * 
	 * return @{@link Product}
	 */
	@Override
	public List<Product> getProductsList() {
		return productRepository.findAll();
	}

	/**
	 * Method to return the product details for @{id}
	 * 
	 * return @{@link Product}
	 */
	@Override
	public Product getProductByProductId(Long id) {
		if (logger.isDebugEnabled())
			logger.debug("Get Product by id [{}]", id);
		return productRepository.findById(id).orElseThrow(() -> new NotFoundException("Not found product"));
	}

	/**
	 * Method to update the product details for @{id}
	 * 
	 * return @{@link Product}
	 */
	public Product updateProductById(Long id) {
		if (!productRepository.existsById(id)) {
			if (logger.isErrorEnabled())
				logger.error("trying to update non existing product. Candidate product for update [{}]", id);
			throw new IllegalOperationException("Illegal operation");
		}
		Product product = productRepository.findById(id).get();
		product.setLastUpdatedTime(LocalDateTime.now());
		return productRepository.save(product);
	}

	/**
	 * Method to create the product details for @{id}
	 * 
	 * return @{@link Product}
	 */
	@Override
	public Product createProduct(Product product) {
		productValidationService.validateProductId(product);
		if (productRepository.existsById(product.getId())) {
			if (logger.isErrorEnabled())
				logger.error("trying to add an existing product. Candidate product for creation [{}]", product);
			throw new IllegalOperationException("Illegal operation");
		}
		product.setLastUpdatedTime(null);
		return productRepository.save(product);
	}

}
