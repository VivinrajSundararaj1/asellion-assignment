package com.asellion.assign.service;

import java.util.List;

import com.asellion.assign.entity.Product;
import com.asellion.assign.service.impl.ProductServiceImpl;

/**
 * Interface class for retrieving @{@link ProductServiceImpl}
 *
 */
public interface ProductService {

	List<Product> getProductsList();

	Product getProductByProductId(Long id);

	Product updateProductById(Long id);

	Product createProduct(Product product);

}
