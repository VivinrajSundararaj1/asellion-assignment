package com.asellion.assign.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.asellion.assign.entity.Product;

/**
 * Repository for @{@link Product}
 *
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long> {

}
