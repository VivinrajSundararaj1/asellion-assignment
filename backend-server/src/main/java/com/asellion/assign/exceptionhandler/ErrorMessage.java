package com.asellion.assign.exceptionhandler;

import lombok.Data;

/**
 * Error message Object
 *
 */

@Data
public class ErrorMessage {
	private int code;
	private String message;

	public ErrorMessage(int code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

}
