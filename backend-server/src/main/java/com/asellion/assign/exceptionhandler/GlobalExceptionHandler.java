package com.asellion.assign.exceptionhandler;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.asellion.assign.exception.IllegalOperationException;
import com.asellion.assign.exception.NotFoundException;

/**
 * Global Exception Handler
 *
 */
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	public GlobalExceptionHandler() {
		super();
	}

	@ExceptionHandler(NotFoundException.class)
	protected ResponseEntity<Object> handlerNotFoundException(NotFoundException ex, WebRequest request) {
		return handleException(1, ex, request);
	}

	@ExceptionHandler(IllegalOperationException.class)
	protected ResponseEntity<Object> handlerIllegalOperationException(IllegalOperationException ex,
			WebRequest request) {
		return handleException(2, ex, request);
	}

	@ExceptionHandler(IllegalArgumentException.class)
	protected ResponseEntity<Object> handlerIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {
		return handleException(3, ex, request);
	}

	@ExceptionHandler(HttpServerErrorException.class)
	protected ResponseEntity<Object> handlerHttpServerErrorException(HttpServerErrorException ex, WebRequest request) {
		return handleException(4, ex, request, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private ResponseEntity<Object> handleException(int errorCode, RuntimeException ex, WebRequest request) {
		return handleException(errorCode, ex, request, HttpStatus.BAD_REQUEST);
	}

	private ResponseEntity<Object> handleException(int errorCode, RuntimeException ex, WebRequest request,
			HttpStatus httpStatus) {
		return handleExceptionInternal(ex, new ErrorMessage(errorCode, ex.getMessage()), new HttpHeaders(), httpStatus,
				request);
	}
}
