package com.asellion.assign.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.asellion.assign.entity.Product;
import com.asellion.assign.service.ProductService;

/**
 * Rest Controller class to handle @{@link ProductService} related rest calls
 * 
 * @author Vivinraj Sundararaj
 *
 */
@RestController
@RequestMapping("/api/products")
public class ProductController {

	Logger logger = LogManager.getLogger(this.getClass());

	@Autowired
	ProductService productService;

	/**
	 * Method to handle GET request for pulling all products
	 * 
	 * @return ResponseEntity
	 */
	@GetMapping
	@ResponseBody
	public ResponseEntity<List<Product>> getProducts() {
		logger.info("call getProducts api");
		return new ResponseEntity<>(productService.getProductsList(), HttpStatus.OK);
	}

	/**
	 * Method to handle GET request for particular product
	 * 
	 * @param id @{@link ProductService}
	 * @return
	 */
	@GetMapping(value = "/{id}", produces = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE })
	@ResponseBody
	public ResponseEntity getProductById(@PathVariable("id") Long id) {
		logger.info("call getProductById with params [{}]", productService.getProductByProductId(id));
		return new ResponseEntity<>(productService.getProductByProductId(id), HttpStatus.OK);
	}

	/**
	 * Method to handle PUT request to change value of particular product
	 * 
	 * @param id @{@link ProductService}
	 * @return
	 */
	@PutMapping("/{id}")
	@ResponseBody
	public ResponseEntity updateProduct(@PathVariable("id") Long id) {
		logger.info("call updateProduct with params [{}]", id);
		return new ResponseEntity<>(productService.updateProductById(id), HttpStatus.OK);
	}

	/**
	 * Method to handle the POST request to add products
	 * 
	 * @param product @{@link ProductService}
	 * @return
	 */
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity createProduct(@RequestBody Product product) {
		logger.info("call createProduct with params [{}]", product);
		return new ResponseEntity<>(productService.createProduct(product), HttpStatus.CREATED);
	}

}
