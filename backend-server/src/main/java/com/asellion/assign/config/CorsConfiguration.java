package com.asellion.assign.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Cross Origin Resource Sharing configuration class
 */
@Configuration
public class CorsConfiguration {

	@Value("${allowedOrigins}")
	private String allowedOrigins;

	/**
	 * Allow front end to access api end point
	 * 
	 * @return cors web configuration
	 */
	@Bean
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/api/**").allowedOrigins(allowedOrigins.split(","));
			}
		};
	}

}