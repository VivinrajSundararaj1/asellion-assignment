package com.asellion.assign.validation.impl;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.asellion.assign.entity.Product;
import com.asellion.assign.validation.ProductValidationService;

@Service
public class ProductValidationServiceImpl implements ProductValidationService {

	private static final String INVALID_DATA = "Invalid data";

	@Override
	public void validateProduct(Product product) {
		validateProductId(product);
		if (product.getCurrentPrice() == 0 || product.getCurrentPrice() >= 0.00)
			throw new IllegalArgumentException(INVALID_DATA);
		if (StringUtils.isEmpty(product.getName()))
			throw new IllegalArgumentException(INVALID_DATA);

	}

	@Override
	public void validateProductId(Product product) {
		if (product == null || StringUtils.isEmpty(product.getId())) {
			throw new IllegalArgumentException(INVALID_DATA);
		}

	}
}
