package com.asellion.assign.validation;

import com.asellion.assign.entity.Product;
import com.asellion.assign.validation.impl.ProductValidationServiceImpl;

/**
 * Interface Class for validating product @{@link ProductValidationServiceImpl}
 *
 */
public interface ProductValidationService {

	void validateProduct(Product product);

	void validateProductId(Product product);
}
