package com.asellion.assign.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Product entity DTO class
 *
 */
@Entity
@Table(name = "product_details", catalog = "product")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "current_price", nullable = false)
	private double currentPrice;

	@Column(name = "last_updated_time")
	private LocalDateTime lastUpdatedTime;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public LocalDateTime getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(LocalDateTime lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	/**
	 * Constructor with field parameters
	 * 
	 * @param id
	 * @param name
	 * @param currentPrice
	 * @param lastUpdatedTime
	 */
	public Product(Long id, String name, double currentPrice, LocalDateTime lastUpdatedTime) {
		super();
		this.id = id;
		this.name = name;
		this.currentPrice = currentPrice;
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

}
