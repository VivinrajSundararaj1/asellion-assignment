package com.asellion.assign;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

import com.asellion.assign.entity.Product;
import com.asellion.assign.repository.ProductRepository;

/**
 * 
 * Spring Boot Starter application start point
 *
 */

@SpringBootApplication
@EntityScan(basePackages = { "com.asellion.assign.entity" })
public class AsellionAssignmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(AsellionAssignmentApplication.class, args);
	}

	/**
	 * Add sample product for demo
	 * 
	 * @param productRepository
	 * @return
	 */
	@Bean
	public CommandLineRunner demo(ProductRepository productRepository) {
		return (args) -> {
			// Add 1 initial chemical Product
			productRepository.save(new Product(Long.valueOf(1), "Baking Powder (NaHCO3)", 12.00, null));
			productRepository.save(new Product(Long.valueOf(2), "Propylene", 20.00, null));
		};
	}

}
