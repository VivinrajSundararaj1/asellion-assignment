import React, { Component } from "react";
import { ProductListPage } from "./container/ProductListPage/ProductListPage.js";
import LoginComponent from './components/LoginComponent/LoginComponent.jsx';
import AuthenticatedRoute from './components/Authentication/AuthenticatedRoute.jsx';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>  
      
        <Switch>
          <AuthenticatedRoute path='/' exact component={ProductListPage}/>
          <Route path="/login" exact component={LoginComponent} />
        </Switch>
      
      </Router>
    )
  }
}

export default App;