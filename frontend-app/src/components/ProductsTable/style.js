import styled from "styled-components";

export const FlexContainer = styled.div`
  padding-top: 20px;
  display: flex;
  flex-direction: column;
  font-family: Arial, sans-serif;
  font-size: 500;
  text-align: left;
  flex: 1;
  .row {
    display: flex;
  }
`;

export const TableContainer = styled.div`
  margin-bottom: 5rem;
  margin-top: 1rem;
  padding: 0 1rem;
  background: rgb(255, 255, 255);
  border: solid rgb(200, 204, 214);
  font-size: 0.75rem;
`;

export const DetailContent = styled.div`
  color: blue;
`;

export const TableRow = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 10px 0;
  flex-direction: row;
  border-top: 1px solid black;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const TableImage = styled.img`
  width: 100px;
  padding: 10px;
  height: 100px;
`;

export const TableCell = styled.div`
  width: calc(100% / 4);
  display: flex;
  justify-content: center;
  border: solid rgb(200, 204, 214);
  @media (max-width: 768px) {
    width: 100%;
  }
`;