import React from "react";
import { DetailContent, FlexContainer, TableContainer, TableRow, TableCell } from "./style.js";

const ProductRow = ({product}) => {
    return (
        <TableRow>
            <TableCell>
                <DetailContent>{product.id}</DetailContent>
            </TableCell>
            <TableCell>
                <DetailContent>{product.name}</DetailContent>
            </TableCell>
            <TableCell>
                <DetailContent>{product.currentPrice}</DetailContent>
            </TableCell>
            <TableCell>{product.lastUpdatedTime === null ? "" : product.lastUpdatedTime}</TableCell>
        </TableRow>
    );
};

export const ProductsCells = ({ products }) => {
    return (
        <>
            <TableRow>
                <TableCell>Product Identifier</TableCell>
                <TableCell>Product name</TableCell>
                <TableCell>Current Price</TableCell>
                <TableCell>Last Update data</TableCell>
            </TableRow>{" "}
            {products.map(product => {
                return <ProductRow product={product} key={product}/>;
            })}
        </>
    );
};

export const ProductsTable = (props) => {
    return (
        <div>
            <h5>Products list</h5>
            <FlexContainer>
                <TableContainer>
                    <ProductsCells products={props.products} />
                </TableContainer>
            </FlexContainer>
        </div>
    );
};