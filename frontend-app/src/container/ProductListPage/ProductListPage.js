import React, { Component } from "react";
import ProductListService from "./api";
import { ProductsTable } from "../../components/ProductsTable/ProductsTable.js"
import AuthenticationService from "../../components/Authentication/AuthenticationService";

const fetchProductList = async (e) => {
    try {
        const response = await ProductListService.fetchProductsList();
        return response.data;
    } catch (error) {
        return (error.data);
    }
};

export class ProductListPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: []
        };
    }

    componentDidMount() {   
        if (AuthenticationService.isUserLoggedIn()) {
            AuthenticationService.registerSuccessfulLoginForJwt(AuthenticationService.getLoggedInUserName, sessionStorage.getItem('token'));
            fetchProductList().then(result => {
                this.setState({
                    products: result
                })
            });
        }
    }

    render() {
        return (            
            <ProductsTable products={this.state.products} />
        );
    }
}

export default ProductListPage;