import axios from "axios";

const API_URL =
    (window && window.env && window.env.API_URL) || process.env.REACT_APP_API_URL;


export default class ProductListService {
   
    
    static fetchProductsList = () => {
        console.log('Executed /api/products GET')
        return axios.get(`${API_URL}/api/products`,
        { Headers:{authorization: 'Bearer ' + sessionStorage.getItem('token')}}
        );
    };
}